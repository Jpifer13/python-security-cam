from adafruit_vc0706 import VC0706
from micropython import const
import os

__version__ = "0.0.0-auto.0"
__repo__ = "https://github.com/adafruit/Adafruit_CircuitPython_VC0706.git"

# pylint: disable=bad-whitespace
_SERIAL = const(0x00)
_RESET = const(0x26)
_GEN_VERSION = const(0x11)
_SET_PORT = const(0x24)
_READ_FBUF = const(0x32)
_GET_FBUF_LEN = const(0x34)
_FBUF_CTRL = const(0x36)
_DOWNSIZE_CTRL = const(0x54)
_DOWNSIZE_STATUS = const(0x55)
_READ_DATA = const(0x30)
_WRITE_DATA = const(0x31)
_COMM_MOTION_CTRL = const(0x37)
_COMM_MOTION_STATUS = const(0x38)
_COMM_MOTION_DETECTED = const(0x39)
_MOTION_CTRL = const(0x42)
_MOTION_STATUS = const(0x43)
_TVOUT_CTRL = const(0x44)
_OSD_ADD_CHAR = const(0x45)

_STOPCURRENTFRAME = const(0x0)
_STOPNEXTFRAME = const(0x1)
_RESUMEFRAME = const(0x3)
_STEPFRAME = const(0x2)

# pylint doesn't like the lowercase x but it makes it more readable.
# pylint: disable=invalid-name
IMAGE_SIZE_640x480 = const(0x00)
IMAGE_SIZE_320x240 = const(0x11)
IMAGE_SIZE_160x120 = const(0x22)
# pylint: enable=invalid-name
_BAUDRATE_9600 = const(0xAEC8)
_BAUDRATE_19200 = const(0x56E4)
_BAUDRATE_38400 = const(0x2AF2)
_BAUDRATE_57600 = const(0x1C1C)
_BAUDRATE_115200 = const(0x0DA6)

_MOTIONCONTROL = const(0x0)
_UARTMOTION = const(0x01)
_ACTIVATEMOTION = const(0x01)

__SET_ZOOM = const(0x52)
__GET_ZOOM = const(0x53)

_CAMERA_DELAY = const(10)
# pylint: enable=bad-whitespace


class VC0706_Video_Wrapper( VC0706 ):
	"""
	This is a wrapper to the class VC0706 that implements recording of videos
	"""

	def __init__(self, uart, *, buffer_size=1000):
            self._uart = uart
            self._buffer = bytearray(buffer_size)
            self._frame_ptr = 0
            self._command_header = bytearray(3)

            # Make sure that port is open
            if self._uart.is_open:
                print(f'Port: {self._uart.port} is open.')
            else:
                print(f'Port: {self._uart.port} is not open.')

#            for _ in range(2):  # 2 retries to reset then check resetted baudrate
#                for baud in (9600, 19200, 38400, 57600, 115200):
#                    print(baud)
                self._uart.baudrate = baud
                try:
                    self._run_command(_RESET, b"\x00", 5)
                except Exception as err:
                    print(err)
#                if self._run_command(_RESET, b"\x00", 5):
#                        break
#                else:  # for:else rocks! http://book.pythontips.com/en/latest/for_-_else.html
#                    raise RuntimeError("Failed to get response from VC0706, check wiring!")
	
	@property
	def get_motion_status(self):
		self._send_command(_COMM_MOTION_STATUS, b"\x01")
		readlen = self._read_response(self._buffer, len(self._buffer))
		return str(self._buffer[:readlen], "ascii")

	def __save_to_video(self, video_in_bytes):
		print('Saving to file...')
		FILE_OUTPUT = 'output.avi'

		# Checks and deletes the output file
		# You cant have a existing file or it will through an error
		if os.path.isfile(FILE_OUTPUT):
			os.remove(FILE_OUTPUT)

		# opens the file 'output.avi' which is accessable as 'out_file'
		with open(FILE_OUTPUT, "wb") as out_file:  # open for [w]riting as [b]inary
			out_file.write(video_in_bytes)

	def take_video(self):
#		b = memoryview(self._buffer)[0:len(self._buffer)]
		b = self._buffer
		print('Recording video....')
		arr = self._uart.readinto(memoryview(b)[0:len(b)])
		print(arr)
#		if b == self._buffer:
#			print(b)
#		self.__save_to_video(b)
