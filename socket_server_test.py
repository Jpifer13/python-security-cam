import socket
import os
import time
import busio
import board
import adafruit_vc0706

# Create a serial connection for the VC0706 connection.
import serial
uart = serial.Serial("/dev/ttyS0", baudrate=115200, timeout=0.25)

# AF_INET corresponds to IPV4 and SOCK_STREAM is TCP
s = socket.socket( socket.AF_INET, socket.SOCK_STREAM )

s.bind(( '192.168.1.237', 8082))
s.listen(5)

# Setup VC0706 camera
vc0706 = adafruit_vc0706.VC0706(uart, buffer_size=50000)

# Print the version string from the camera.
print("VC0706 version:")
print(vc0706.version)

# Set the image size.
vc0706.image_size = adafruit_vc0706.IMAGE_SIZE_640x480
# Or set IMAGE_SIZE_320x240 or IMAGE_SIZE_160x120

# Take a picture.
print("SNAP!")
if not vc0706.take_picture():
    raise RuntimeError("Failed to take picture!")

# Print size of picture in bytes.
frame_length = vc0706.frame_length
print("Picture size (bytes): {}".format(frame_length))


while True:
    clientsocket, address = s.accept()
    print("Connection from " + str(address) + "has been established!")
    clientsocket.send(bytes("Welcome to the server!", "utf-8"))
    stamp = time.monotonic()
    wcount = 0
    print("Sending bytes!\n")
    while frame_length > 0:
        t = time.monotonic()
        # Compute how much data is left to read as the lesser of remaining bytes
        # or the copy buffer size (32 bytes at a time).  Buffer size MUST be
        # a multiple of 4 and under 100.  Stick with 32!
        to_read = min(frame_length, 44000) # read length either frame_length or 32
        # depending on which is smaller
        copy_buffer = bytearray(to_read) #
        # Read picture data into the copy buffer.
        if vc0706.read_picture_into(copy_buffer) == 0:
            raise RuntimeError("Failed to read picture frame data!")
        # Send data through socket
        clientsocket.send( copy_buffer )
        frame_length -= 32
        # Print a dot every 2k bytes to show progress.
        wcount += 1
        if wcount >= 64:
            print(".", end="", flush=True)
            wcount = 0
    clientsocket.close()
    print('\n')
    break

print("Finished in %0.1f seconds!" % (time.monotonic() - stamp))
print('Connection closed.')

