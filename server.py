import socket
import os

_IP = os.getenv('SEC_IP')
_PORT = os.getenv('SEC_PORT')

# AF_INET corresponds to IPV4 and SOCK_STREAM is TCP
s = socket.socket( socket.AF_INET, socket.SOCK_STREAM )

s.bind(( '192.168.1.90', 8080))
s.listen(5)

while True:
    clientsocket, address = s.accept()
    print("Connection from " + str(address) + "has been established!")
    clientsocket.send(bytes("Welcome to the server!", "utf-8"))