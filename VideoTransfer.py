import os
import socket

def receive_msg(socket, buffer=1024) -> str:
    return socket.recv( buffer ).decode('utf-8')


def receive_stream(socket, buffer=1024):
    frame_arr = []
    while True:
        copy_buff = socket.recv( buffer )
        frame_arr.append( copy_buff )
        if len( copy_buff ) == 0:
            break
    print(frame_arr)
